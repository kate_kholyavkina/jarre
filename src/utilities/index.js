import {Dimensions, Platform, PixelRatio} from 'react-native';

const {width, height} = Dimensions.get('window');

export const normalize = (size) => {
    // based on iphone 6 scale(as on design)
    const scale = width / 750;
    return PixelRatio.roundToNearestPixel(size * scale);
};