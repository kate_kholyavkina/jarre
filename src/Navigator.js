import React from 'react';
import {StackNavigator} from 'react-navigation';
import WelcomeScreen from './screens/WelcomeScreen';
import WebpageScreen from './screens/WebpageScreen';

const RootNavigator = StackNavigator({
    Welcome: {
      screen: WelcomeScreen,
      navigationOptions: {
        header: null,
      }
    },
    Webpage: {
      screen: WebpageScreen,
      navigationOptions: {
        header: null,
      },
    },
});

export default RootNavigator;