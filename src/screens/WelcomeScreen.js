import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import Images from './../config/images';
import {normalize} from './../utilities';
import {fonts} from './../styles/index';

export default class WelcomeScreen extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.logo} source={Images.jarreLogo} resizeMode={'contain'}/>
                <View style={styles.buttonsHolder}>
                    <TouchableOpacity style={styles.button} onPress={() => {this.props.navigation.navigate('Webpage')}}>
                        <Text style={styles.buttonText}> ДАЛІ </Text>
                        <Image style={styles.buttonArrow} source={Images.arrow} resizeMode={'contain'}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    logo: {
        width: normalize(567),
        height: normalize(282),
    },
    buttonsHolder: {
        marginTop: normalize(410),
    },
    button: {
        borderWidth: normalize(3),
        width: normalize(457),
        height: normalize(94),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    buttonText: {
        fontSize: normalize(36),
        color: '#000',
        textAlign: 'center',
        fontFamily: fonts.latoBold,
    },
    buttonArrow: {
        width: normalize(17),
        height: normalize(26),
        marginTop: normalize(2),
        marginLeft: normalize(8),
    },
});
  