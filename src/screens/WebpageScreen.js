import React, {Component} from 'react';
import {Text, View, WebView} from 'react-native';

export default class WebpageScreen extends Component {

    render() {
        return (
            <WebView
              source={{uri: 'http://jarre.com.ua/'}}
            />
        )
    }
}
