export const fonts = {
    latoBold: 'Lato Bold',
    latoMedium: 'Lato Medium',
    latoRegular: 'Lato Regular',
}